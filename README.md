# React Multi Step Form

In this task we are going to use some exciting features of react like :-

- react-router
- hooks
- reducers
  and many more things...

## How to use it?

Just clone this repo and run following commands:-

```
cd 'repo_name'
npm install or yarn
npm start or yarn start
```

**here is the deployed version link:-**

"https://react-multistep-form-seven.vercel.app/"
