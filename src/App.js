const initialFormData = {
  fName: '',
  lName: '',
  dateOfBirth: "",
  email: '',
  address: '',
  bigMessage: '',
  mChoice1: 0,
  mChoice2: 1,
  male: 0,
  female: 1,
  bChoice1: 0,
  bChoice2: 0,
};

const wrongFormData = {
  wrongFName: '',
  wrongLName: '',
  wrongDateOfBirth: '',
  wrongEmail: '',
  wrongAddress: '',
};

export {initialFormData, wrongFormData};